#! /bin/bash
# deploy.sh
# Written by: Cary Howell <james.c.howell@accenture.com>
# Purpose   : Deployment or validation shell script for ADOA
# Date      : 24-Sept-2015
# Version   : 1.1.0
VERSION="1.1.0"
#
# constants : required constants and arrays
#
NORM=`tput sgr0`
BOLD=`tput bold`
REVS=`tput smso`
NAME=`basename ${BASH_SOURCE[0]}`
declare -a SANDBOXES=("FB3" "QA" "IDL" "LATEST" "STAGE" "SHADOW" "STRESS" "TRAIN" "PROD")
WORKSPACE="/opt/Repos/"
REPO="dvc-sfdc-deploy"
GIT_BRANCH="develop"
SANDBOX="LATEST"
ANT="/usr/bin/ant"
PackageToValidate="Main_Package"
SCRIPT_HOME="/opt/ADOA/Deploy"
#
# function  : usage() to show command line options as needed.
#
usage() {
   echo -e \\n"Usage: ${BOLD}${NAME}${NORM} -w <workspace> -b <branch> -s <sandbox> [-d] [-h]"
   echo -e "          -w Path to the filesystem workspace"
   echo -e "          -r optional name of the repository"
   echo -e "          -b Branch of SFDC repository to validate"
   echo -e "             > develop|release|master"
   echo -e "          -s Target SFDC environment to deploy to"
   echo -e "             > FB3|QA|LATEST|STAGE|SHADOW|STRESS|TRAIN|PROD"
   echo -e "          -d option requests a destructive deployment"
   echo -e "          -v option requests that we validate only"
   echo -e "          -h option displays this help screen"\\n
   exit 1
}
#
# function  : doesContain() to verify if an element is found in an array
#
function doesContain() {
   local e
   for e in "${@:2}"; do [[ "$e" == "$1" ]] && return 0; done
   return 1
}

# script    : START
#           : preset the PackageToValidate to Main_Package and only change
#           : it when the -d option is on the command line making it a 
#           : destructive.
while getopts ":w:r:b:s:dhv" o; do
   case "${o}" in
      w) WORKSPACE=${OPTARG}
      ;;
      b) GIT_BRANCH=${OPTARG}
      ;;
      d) PackageToValidate="Destructive"
      ;;
      h) usage
      ;;
      s) SANDBOX=${OPTARG}
      ;;
      r) REPO=${OPTARG}
      ;;
      v) VALIDATION="startFullValidationProcess"
      ;;
      :) echo -e "${REVS}Option -$OPTARG requires an argument${NORM}" >&2
         usage
      ;;
      \?) echo "${REVS}Invalid option ${OPTARG}${NORM}" >&2
          usage
      ;;
      *) usage
      ;;
   esac
done
#
# show header
#
echo -e "------------------------------------------------------------------"
echo -e "${BOLD}${NAME}${NORM} v.${VERSION} : ${BOLD}ADOA for GitHub & Salesforce.com${NORM}"
echo -e "------------------------------------------------------------------"
#
# is the sandbox a valid one?
#
doesContain "$SANDBOX" "${SANDBOXES[@]}"
if [[ $? -eq 1 ]]; then
   echo -e \\n"${BOLD}${SANDBOX}${NORM} is not a valid SFDC location!"
   usage
fi
# Find out which branch we are on
WORKDIR="${WORKSPACE}${REPO}"
cd $WORKDIR
echo -e "Updating ${BOLD}${REPO}${NORM} repository"
GITFIX=$(/usr/bin/git pull)
BRANCH="$(/usr/bin/git symbolic-ref -q HEAD)"
BRANCH=${BRANCH##refs/heads/}
BRANCH=${BRANCH:-HEAD}
echo $GITFIX
echo -e "------------------------------------------------------------------"
# Report what we're going to be doing.
echo -e "SFDC location is set to: ${BOLD}${SANDBOX}${NORM}"
echo -e "Local repository branch: ${BOLD}${BRANCH}${NORM}"
if [ -z $VALIDATION ]; then
   echo -e "Deployment from branch: ${BOLD}${GIT_BRANCH}${NORM}"
   echo -e "Validate location only: ${BOLD}No${NORM}"
else
   echo -e "Validating from branch: ${BOLD}${GIT_BRANCH}${NORM}"
   echo -e "Validate location only: ${BOLD}Yes${NORM}"
fi
echo -e "Type of work to be done: ${BOLD}${PackageToValidate}${NORM}"
echo -e "Local repository workspace: ${BOLD}${WORKDIR}${NORM}"
export WORKSPACE=${WORKDIR}
export SANDBOX=${SANDBOX}
export GIT_BRANCH=${GIT_BRANCH}
export PackageToValidate=${PackageToValidate}

# Start the deployment process
if [ -z "$VALIDATION" ]; then
  read -n1 -r -p "Press [Y] to start deployment, any other key to abort. " key
else
  read -n1 -r -p "Press [Y] to start validation, any other key to abort. " key
fi
echo 
if [ "$key" == "Y" ]; then
   $(/usr/bin/git checkout $GIT_BRANCH)
   echo -e "Local branch set to ${BOLD}${GIT_BRANCH}${NORM}"
else
   cd $PWD
   echo -e "${BOLD}Job aborted by user.${NORM}"\\n
   exit 0
fi
cd $SCRIPT_HOME
$ANT -buildfile /opt/ADOA/Deploy/build.xml -DSandbox=${SANDBOX} -DPackageToValidate=${PackageToValidate} ${VALIDATION}
echo -e \\n"${BOLD}Job completed.${NORM}"\\n
# script    : END
