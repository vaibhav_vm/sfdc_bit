// ----------------------------------------------------------------------------------------
// I M P O R T S
// ----------------------------------------------------------------------------------------
import groovy.json.JsonSlurper
import groovy.json.JsonOutput
import GitHubController

// Read configuration
def parser = new JsonSlurper()
def automaticJobsText = new File('/opt/ADOA/JenkinsDefinition/automatic_jobs.json').text
println  automaticJobsText
def automaticJobsJson = parser.parseText(automaticJobsText)
List branchList = automaticJobsJson.branches.keySet() as List
def host = automaticJobsJson.host
def sfdcRepository = automaticJobsJson.sfdcRepository
def gitHubProtocol = automaticJobsJson.gitHubProtocol
def releaseManagers = automaticJobsJson.releaseManagers
def sandboxList = automaticJobsJson.sandboxList
def reportTime = automaticJobsJson.reportTime
def distributionPackages = automaticJobsJson.distributionPackages

// ----------------------------------------------------------------------------------------
// R E U S A B L E  - C O M P O N E N T S
// ----------------------------------------------------------------------------------------
def deployParams = {
    choiceParam("GIT_BRANCH", branchList, "Git Branch used to create the deployment package.")
    choiceParam("Sandbox", sandboxList, "Salesforce Environment for the deployment package.")
    choiceParam("PackageToValidate", distributionPackages, "Deploy a package or destructive changes.")
}

def validationParams = {
    choiceParam("GIT_BRANCH", branchList, "Git Branch used to create the validation package.")
    choiceParam("Sandbox", sandboxList, "Salesforce Environment for the validation package.")
    choiceParam("PackageToValidate", distributionPackages, "Validate a package or destructive changes.")
}
def loadParams = {
    stringParam("username")
    stringParam("password")
}
def gitDefinition = { repo ->
    git {
      remote {
        github(repo, gitHubProtocol ,  host)
        wipeOutWorkspace(true)
      }
    }
}

def gitsfdcRepository = gitDefinition.curry(sfdcRepository)

def stepsDefinition = {
  folder, target, before ={} ->
  return {
    before
    ant(target) {
      buildFile "/opt/ADOA/${folder}/build.xml"
    }
  }
}

def maskPasswords = {
  maskPasswords()
}

// ----------------------------------------------------------------------------------------
// J O B S - D E F I N I T I O N
// ----------------------------------------------------------------------------------------
job ('On Demand Deploy') {
  description """
  Create a deployment package from a Git branch and deploy it to a Salesforce environment.
  Important considerations:
  This job uses the Git branch version.txt to determine the first commit, where only modified files are copied to the deployment package.
  All files in the deployment package must be listed in package.xml, and all files listed in package.xml must be in the deployment package.
  """
  parameters deployParams
  scm gitsfdcRepository
  steps stepsDefinition('Deploy', "")
}

job ('On Demand Validation' ) {
  description """
  Create a validation package from a Git branch and validate it against a Salesforce environment.
  Important considerations:
  This job uses the Git branch version.txt to determine the first commit, where only modified files are copied to the validation package.
  All files in the validation package must be listed in package.xml, and all files listed in package.xml must be in the validation package.
  The validation tests that the deployment will succeed and all Apex test classes will pass.
  """
  parameters validationParams
  scm gitsfdcRepository
  steps stepsDefinition('Deploy', 'startFullValidationProcess')
}

// ----------------------------------------------------------------------------------------
//  HACK
// ----------------------------------------------------------------------------------------
// Update succesMsg and comment file on pull request trigger

import javaposse.jobdsl.dsl.DslContext
import javaposse.jobdsl.dsl.helpers.triggers.PullRequestBuilderContext
import javaposse.jobdsl.dsl.ContextHelper
javaposse.jobdsl.dsl.helpers.triggers.TriggerContext.metaClass.pullRequest = {
         Closure contextClosure ->
        PullRequestBuilderContext pullRequestBuilderContext = new PullRequestBuilderContext()
        ContextHelper.executeInContext(contextClosure, pullRequestBuilderContext)

        triggerNodes << new NodeBuilder().'org.jenkinsci.plugins.ghprb.GhprbTrigger' {
            adminlist pullRequestBuilderContext.admins.join('\n')
            whitelist pullRequestBuilderContext.userWhitelist.join('\n')
            orgslist pullRequestBuilderContext.orgWhitelist.join('\n')
            delegate.cron(pullRequestBuilderContext.cron)
            spec pullRequestBuilderContext.cron
            triggerPhrase pullRequestBuilderContext.triggerPhrase
            onlyTriggerPhrase pullRequestBuilderContext.onlyTriggerPhrase
            useGitHubHooks pullRequestBuilderContext.useGitHubHooks
            permitAll pullRequestBuilderContext.permitAll
            autoCloseFailedPullRequests pullRequestBuilderContext.autoCloseFailedPullRequests
            commentFilePath '$WORKSPACE/validate.log'
        }
}
// ----------------------------------------------------------------------------------------
//  HACK
// ----------------------------------------------------------------------------------------
// Merge pull request if success
javaposse.jobdsl.dsl.helpers.publisher.PublisherContext.metaClass.pullRequestMerge = {
  NodeBuilder nodeBuilder = NodeBuilder.newInstance()
  Node pullRequestMerge = nodeBuilder.'org.jenkinsci.plugins.ghprb.GhprbPullRequestMerge' {
      onlyAdminsMerge=false
      disallowOwnCode=false
      onlyTriggerPhrase=false
      mergeComment='Merge complete'
  }
  publisherNodes << pullRequestMerge
}
job ('Pull Request Validation') {
  description """
  Execute a validation without Apex test classes for a specific Pull Request against a Salesforce environment.
  """
  scm {
    git {
      remote {
        github(sfdcRepository, gitHubProtocol ,  host)
        wipeOutWorkspace(true)
        refspec('+refs/pull/*:refs/remotes/origin/pr/*')
     }
     branch('${sha1}')
    }
   }

  triggers {
    pullRequest {
      orgWhitelist('Salesforce')
      cron('H/5 * * * *')
      permitAll()
    }
  }
  steps {
    ant('pullRequestValidation') {
      props 'author':'$ghprbActualCommitAuthor'
      buildFile "/opt/ADOA/Deploy/build.xml"
    }
  }
}
job ('Pull Request Deploy') {
  description """
  Execute a deployment for a specific Pull Request against a Salesforce environment.
  """
  scm {
    git {
      remote {
        github(sfdcRepository, gitHubProtocol ,  host)
        wipeOutWorkspace(true)
        refspec('+refs/pull/*:refs/remotes/origin/pr/*')
     }
     branch('${sha1}')
    }
   }

  triggers {
    pullRequest {
      admins(releaseManagers)
      triggerPhrase('deploy this')
      onlyTriggerPhrase(true)
      orgWhitelist('Salesforce')
      cron('H/5 * * * *')
      permitAll()
    }
  }
  steps {
    ant('pullRequestDeploy') {
      props 'author':'$ghprbActualCommitAuthor'
      buildFile "/opt/ADOA/Deploy/build.xml"
    }
  }
}

automaticJobsJson.branches.each {

  def branchName = it.key
  def test_crontab = it.value.unit_test_time
  def deploy_crontab = it.value.deploy_time

  if( test_crontab ) {

    test_crontab.each{
      def sandbox = it.sandbox
      def assignee = it.assignee
      def time = it.time
	  def PackageToValidate = it.PackageToValidate
      job ("Scheduled Validate ${branchName} to ${sandbox}") {
        parameters {
          stringParam('assignee', "${assignee}")
        }
        description """
        Validate Git branch $branchName against the $sandbox Salesforce environment.
        """
        scm {
          git {
            remote {
              github(sfdcRepository, gitHubProtocol ,  host)
              wipeOutWorkspace(true)
           }
           branch("${branchName}")
          }
         }
        triggers {
          scm time
        }
        steps {
          ant('automaticTest') {
            props 'Sandbox':"$sandbox", 'GIT_BRANCH':"$branchName", 'PackageToValidate' : PackageToValidate
            buildFile "/opt/ADOA/Deploy/build.xml"
          }
        }
      }
    }
  }

  if( deploy_crontab ) {

    deploy_crontab.each{
      String sandbox = it.sandbox
      String time = it.time
      String assignee = it.assignee
      String PackageToValidate = it.PackageToValidate

      job ("Scheduled Deploy ${branchName} to ${sandbox}") {
        parameters {
          stringParam('assignee', "${assignee}")
        }
        description """
        Deploy Git branch $branchName to the $sandbox Salesforce environment.
        """
        scm {
          git {
            remote {
              github(sfdcRepository, gitHubProtocol ,  host)
              wipeOutWorkspace(true)
           }
           branch("${branchName}")
          }
         }
        triggers {
          scm time
        }
        steps {
          ant('automaticDeploy') {
            props 'Sandbox':"$sandbox", 'GIT_BRANCH':"$branchName", 'PackageToValidate' : PackageToValidate
            buildFile "/opt/ADOA/Deploy/build.xml"
          }
        }
      }
    }
  }

}
